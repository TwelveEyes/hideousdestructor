//Stuff specific to Freedoom
version "4.6"


#include "zscript/flyingskull.zs"
#include "zscript/keen.zs"

//remove pistol zombie
class ZombieHideousIdTrooper:RandomSpawner replaces ZombieHideousTrooper{
	default{
		dropitem "ZombieAutoStormtrooper",256,100;
		dropitem "ZombieSemiStormtrooper",256,20;
		dropitem "ZombieSMGStormtrooper",256,10;
		dropitem "EnemyHERP",256,1;
	}
}


